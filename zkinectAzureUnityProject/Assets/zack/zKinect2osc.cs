﻿using System;
using System.Collections.Generic;
using UnityEngine;
using OscJack;

namespace LightBuzz.Kinect4Azure
{
    public class zKinect2osc : MonoBehaviour
    {
        [SerializeField] private Configuration configuration;
        [SerializeField] private StickmanManager _stickmanManager;

        
        private OscClient _client;

        static int JOINT_TYPE_COUNT = Enum.GetValues(typeof(JointType)).Length;
        static int MAX_BODS = 10;

        private KinectSensor sensor;
        private List<Stickman> stickmen;
        [SerializeField] private string ipAddress = "localhost";
        [SerializeField] private int udpPort = 54321;
        private Vector3[,] changeReg = new Vector3[MAX_BODS, JOINT_TYPE_COUNT];
        private readonly float MIN_DIFF = 0.005f; // 0.5 centimeter

        private void Start()
        {
            sensor = KinectSensor.Create(configuration);

            if (sensor == null)
            {
                Debug.LogError("Sensor not connected!");
                return;
            }

            sensor.Open();

            _client = OscMaster.GetSharedClient(ipAddress, udpPort);

            for (int i = 0; i < MAX_BODS; i++)
                for (int j = 0; j < JOINT_TYPE_COUNT; j++)
                    changeReg[i, j] = new Vector3(-999f, -999f, -999f);
        }

        public void setIpAddr(string ipAddr)
        {
            ipAddress = ipAddr;
        }

        public void setUDPport(int port)
        {
            udpPort = port;
        }

        public void oscConnect()
        {
            _client?.Dispose();
            _client = OscMaster.GetSharedClient(ipAddress, udpPort);
        }

        private void Update()
        {
            if (sensor == null || !sensor.IsOpen) return;

            Frame frame = sensor.Update();

            if (frame != null)
            {
                List<Body> bodies = frame.BodyFrameSource?.Bodies;

                _stickmanManager.Load(bodies);
                UpdateStickmen(frame.BodyFrameSource.Bodies);
            }
        }
        private void OnDestroy()
        {
            sensor?.Close();
            _client?.Dispose();
        }

        private void UpdateStickmen(List<Body> bodies)
        {
            if (bodies == null)
            {
                return;
            }

            for (int i = 0; i < bodies.Count; i++)
            {
                int jointIndex;
                for (JointType j = 0; j < (JointType)JOINT_TYPE_COUNT; j++)
                {
                    jointIndex = (int)j;



                    if (true) //bodies[i].Joints[j].TrackingState == TrackingState.Medium)
                    {
                        if ( !nearlyEqual (changeReg[i, jointIndex].x, bodies[i].Joints[j].Position.X)
                            ||
                            !nearlyEqual(changeReg[i, jointIndex].y , bodies[i].Joints[j].Position.Y)
                            ||
                            !nearlyEqual(changeReg[i, jointIndex].z , bodies[i].Joints[j].Position.Z ))
                        {
                            changeReg[i, jointIndex].x = bodies[i].Joints[j].Position.X;
                            changeReg[i, jointIndex].y = bodies[i].Joints[j].Position.Y;
                            changeReg[i, jointIndex].z = bodies[i].Joints[j].Position.Z;
                            sendOsc(i, bodies[i].Joints[j].JointType.ToString(), bodies[i].Joints[j].Position);

                            //Debug.Log($"body: {i} joint: {bodies[i].Joints[j].JointType} Z value: {bodies[i].Joints[j].Position.Z} ");                       
                        }
                    }
                    jointIndex++;
                }
            }

            void sendOsc(int bodyNumber, string jointName, Vector3 pos)
            {
                string oscAddr = "/azurKinect/"+bodyNumber+"/" + jointName;
                _client.Send(oscAddr, pos.x, pos.y, pos.z);
/*                if (jointName == JointType.Head.ToString())
                    Debug.Log($"body: {bodyNumber} joint: {jointName} value: {pos} z");*/
            }

            bool nearlyEqual(float a, float b)
            {
                
                if (a == b)
                { // shortcut, handles infinities
                    return true;
                }

                float diff = Math.Abs(a - b);

                if (diff < MIN_DIFF)
                {
                    // a or b is zero or both are extremely close to it
                    // relative error is less meaningful here
                    return true;
                }
                
                return false;
            }
        }
    }
}
